<?php

namespace App\Http\Controllers;

use App\Services\SocialAccountService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;



/**
 * Class SocialAuthController
 * @package App\Http\Controllers
 */
class SocialAuthController extends Controller
{

    /**
     * @param $provider
     * @return mixed
     */
    public function redirect($provider)
    {

        return Socialite::driver($provider)->redirect();
    }


    /**
     * @param SocialAccountService $service
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(SocialAccountService $service, $provider)
    {
        $fbUser = Socialite::with($provider)->stateless()->user();
        
        $user = $service->createOrGetUser($fbUser, $provider);

        Auth::login( $user, true);

        return redirect()->to('/home');
    }
}
